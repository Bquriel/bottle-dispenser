#include "basicFunctions.h"


void flush(){					
	char temp;
	while(( temp = getchar()) != EOF ){
		if (temp == '\n')
			break;
	}
}


void *allocateMemory( int size ){
	void *temp;
	if(( temp = malloc( size )) == NULL){
		printf( "Virhe muistin varaamisessa.\n" );
		exit( -1 );
	}
	return temp;
}


int getInputInRange( int *target, const int rangeStart, const int rangeEnd ){
	char buffer[ BUFFER_SIZE ];
	fgets( buffer, BUFFER_SIZE, stdin );
	const int length = strlen( buffer );
	
	if ( buffer[0] == '\n'){				//Jos k�ytt�j� sy�tt�� vain enterin l�hetet��n virhe.
		*target = -1;
		return 0;
	}
	if ( buffer[ length - 1 ] != '\n' ){	//Tyhjennet��n stdin, jos sy�te on eritt�in pitk�.
		flush();
	}
	
	int validInput = 1;
	*target = atoi( buffer );
	for( int i = 0; i < length - 1; ++i ){	//Tarkistetaan onko sy�te kokonaisluku.
		if( !isdigit( buffer[i] )){	
			validInput = 0;
			*target = -1;
			break;
		}
	}	
	if( *target < rangeStart || *target > rangeEnd ){
		validInput = 0;
	}
	return validInput;
}


void printMenu(){
		printf( "Automaatti on valmis ottamaan vastaan pullot ja t�lkit.\n\n" );
		printf( "1) Aloita palautus\n" );
		printf( "0) Lopeta\n\n" );
		printf( "Valitse: " );
}


void returnMenu( Bottle *root ){
	RetBottle *retRoot = NULL;
	Bottle *toAdd = NULL;
	double sum = 0;
	int num = 0;
	FILE* tempLog = fopen( "tilapaistiedosto.txt", "w" );
	if( tempLog == NULL ){
		printf( "Tiedoston avaaminen ei onnistunut, suljetaan ohjelma\n" );
		exit( -3 );
	}
	fputs( "Tilap�inen lokitiedosto\n\n", tempLog );
	char stamp[ WORD_SIZE ];
	int choice = 9001;
	int isCorrect;
	const int end = bottleGetEndIndex( root ) + 1;			//Haetaan viimeisen pullon j�rjestysnumero ja lasketaan palautuksen lopetuksen sy�te.
	
	while( choice != end ){
		printf( "Sy�t� uusi pullo tai t�lkki.\n\n" );
		bottlePrintList( root );
		printf( "%i) Lopeta sy�tt� ja tulosta kuitti.\n", end );
		printf( "--> " );
		isCorrect = getInputInRange( &choice, 1, end );		//Haetaan sy�te ja tarkistetaan onko se oikea.			
		printf("\n");
		if( !isCorrect ){
			printf( "V��r� valinta!\n\n" );
		}
		else if( choice != end ){							//Lis�t��n palautettu pullo listaan ja p�ivitet��n lokia.
			toAdd = bottleSearch( root, choice );
			retBottleAppendToList( &retRoot, toAdd );
			printf( "Sy�tetty: %s %.2fl\n", toAdd->name, toAdd->size );
			getTimeStamp( stamp );
			fprintf( tempLog, "%s:%s-%.2fl:%.2f�.\n", stamp, toAdd->name, toAdd->size, toAdd->value );
			num++;
			sum += toAdd->value;
		} 
	}
	fclose( tempLog );
	printRecipe( retRoot, num, sum );
	updateMainLog( num, sum );
	retBottleClearList( retRoot );
}


void printRecipe( RetBottle *root, const int num, const float sum ){
	printf( "Kuitti\n\n" );
	printf( "Palautetut pullot ja t�lkit yhteens� %i kappaletta.\n\n", num );
	while( root != NULL ){
		printf( "%s %.2fl\t\tpantit %i x %.2f = %.2f�\n", root->name, root->size, root->count, root->value, root->value * root->count );
		root = root->next;
	}
	printf( "\n\n\nPantit yhteens� %.2f�\n\n\n", sum );
}


void getTimeStamp( char *target){
	//Hakee ajan k�ytt�j�rjestelm�lt� ja purkaa sen structiin.
	time_t ttime;
	struct tm *timeData;
	time( &ttime );
  	timeData = localtime( &ttime );
  	strftime( target, WORD_SIZE, "%d.%m.%Y %H:%M", timeData );	//Antaa ajan oikeassa muodossa argumentille.
}
