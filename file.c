#include "file.h"


void readFile( Bottle **root ){
	FILE *file = fopen( "tuotetiedosto.txt", "r" );
	if( file == NULL ){
		printf( "Tiedoston luku ei onnistunut, suljetaan ohjelma\n" );
		exit( -2 );
	}
	char buffer[ BUFFER_SIZE ];
	char name[ WORD_SIZE ];
	float size, value;
	int errorRow = 1;
	while( fgets( buffer, BUFFER_SIZE, file ) != NULL ){
		//Tarkistetaan onko tuotetiedoston muotoilu oikea ja lis�t��n tuote listaan.
		if( sscanf( buffer, "%s %f %f", name, &size, &value ) == 3 ){
			bottleAppendToList( root, name, size, value );
		} else{
			printf( "Virheellinen muotoilu rivill� %i tiedostossa.\n\n", errorRow );
		}
		errorRow++;
	}
	fclose( file );
}


void updateMainLog( const int num, const double sum ){
	FILE *log = fopen( "lokitiedosto.txt", "a" );
	if( log == NULL ){
		printf( "P��loki-tiedoston avaaminen ei onnistunut, suljetaan ohjelma.\n" );
		exit( -4 );
	}
	char stamp[ WORD_SIZE ];
	getTimeStamp( stamp );
	fprintf( log, "%s - ", stamp );
	fprintf( log, "Palautukset %i kpl. Pantit %.2f�.\n", num, sum );
	fclose( log );
}

