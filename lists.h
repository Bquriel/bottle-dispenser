#pragma once
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <ctype.h>
#define WORD_SIZE 30
#define BUFFER_SIZE 100


typedef struct bottle{
	int 				index;
	char				name[ WORD_SIZE ];
	float				size;
	float				value;
	struct bottle		*next;
} Bottle;


typedef struct retBottle{
	char				name[ WORD_SIZE ];
	int 				index;
	int					count;
	float				size;
	float				value;	
	struct retBottle	*next;
} RetBottle;


/* 
Tyhjent�� tuotetieto-listan.
*/
void bottleClearList( Bottle *root );

/*
Tyhjent�� palautettujen tuotteiden listan.
*/
void retBottleClearList( RetBottle *root );

/*
Lis�� tuotetiedot listaan, tai luo sellaisen jos ei ole jo olemassa.
*/
void bottleAppendToList( Bottle **root, const char name[], const float size, const float val );

/*
Lis�� palautetun pullon tiedot listaa, tai jos sellainen on jo palautettu, kasvattaa palautettujeen m��r��.
*/
void retBottleAppendToList( RetBottle **root, Bottle *toAppend );

/*
Etsii listasta pullon ja palauttaa osoittimen siihen. Paluttaa NULL jos pulloa ei l�ydy.
*/
Bottle *bottleSearch( Bottle *root, int num );

/*
Etsii palautetun puloon listalta ja palauttaa osoittimen siihen. NULL jos ei l�ydy.
*/
RetBottle *retBottleSearch( RetBottle *root, int index );

/*
Funktio hakee viimeisen pullon indeksin, eli j�rjestysnumeron.
*/
int bottleGetEndIndex( Bottle *root );

/*
Tulostaa tuotetiedot ja niiden j�rjestysnumerot palautusvaiheessa.
*/
void bottlePrintList( Bottle *root );
