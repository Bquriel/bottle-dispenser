#include "lists.h"


void bottleClearList( Bottle *root ){
	Bottle *toDelete = root;
	while( root != NULL ){
		root = root->next;
		free( toDelete );
		toDelete = root;
	}
}


void retBottleClearList( RetBottle *root ){
	RetBottle *toDelete = root;
	while( root != NULL ){
		root = root->next;
		free( toDelete );
		toDelete = root;
	}
}


void bottleAppendToList( Bottle **root, const char name[], const float size, const float val ){
	Bottle *temp = *root;
	int num = 1;							//Arvo kuvaa pullon j�rjestysnumeroa listassa.
	if( *root == NULL ){
		*root = allocateMemory( sizeof( Bottle ));
		temp = *root;
	} else{									//Edet��n listan per�lle ja lis�t��n alkio.
		while( temp->next != NULL){
			temp = temp->next;
			num++;
		}
		temp->next = allocateMemory( sizeof( Bottle ));
		temp = temp->next;
		num++;
	}										//Asetetaan arvot.
	strncpy( temp->name, name, WORD_SIZE );
	temp->size = size;
	temp->value = val;
	temp->index = num;
	temp->next = NULL;
}


void retBottleAppendToList( RetBottle **root, Bottle *toAppend ){
	RetBottle *temp = *root;
	if( *root == NULL ){
		*root = allocateMemory( sizeof( RetBottle ));
		temp = *root;
	} else{													//Tarkistetaan onko samanlaista pulloa jo palautettu. Jos on kasvatetaan palautettujen m��r��.
		temp = retBottleSearch( *root, toAppend->index );
		if( temp != NULL ){
			temp->count++;
			return;
		} else{
			temp = *root;
			while( temp->next != NULL ){
				temp = temp->next;
			}
			temp->next = allocateMemory( sizeof( RetBottle ));
			temp = temp->next;
		}
	}
	strncpy( temp->name, toAppend->name, WORD_SIZE );
	temp->count = 1;
	temp->index = toAppend->index;
	temp->size = toAppend->size;
	temp->value = toAppend->value;
	temp->next = NULL;
}



Bottle *bottleSearch( Bottle *root, int num ){
	while( root != NULL ){
		if( root->index == num ){
			return root;
		} else{
			root = root->next;
		}
	}
	return NULL;									//Palautetaan NULL, jos pulloa ei ole listalla.
}



RetBottle *retBottleSearch( RetBottle *root, int index ){
	while( root != NULL ){
		if( root->index == index ){
			return root;
		} else{
			root = root->next;
		}
	}
	return NULL;									//Palautetaan NULL, jos pulloa ei ole listalla.
}



int bottleGetEndIndex( Bottle *root ){
	while( root->next != NULL ){
		root = root->next;
	}
	return root->index;
}



void bottlePrintList( Bottle *root ){
	while( root != NULL ){
		printf( "%i) %s %.2fl\n", root->index, root->name, root->size );
		root = root->next;
	}
}
