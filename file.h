#pragma once
#include "lists.h"
#include "BasicFunctions.h"

/*
Lukee tekstitiedoston linkitettyyn listaan ohjelman alussa.
*/
void readFile( Bottle **root );

/*
Päivittää päälokitiedostoa palautuksen jälkeen.
*/
void updateMainLog( const int num, const double sum );
