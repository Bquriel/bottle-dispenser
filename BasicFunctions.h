#pragma once
#include "lists.h"
#include "file.h"


/*
Funktio tyhjent�� stdini�, mm. virheellisist� sy�tteist�.
*/
void flush();

/*
Void-osoitin toimii geneerisen� osoittimena mihin tahansa tietorakenteeseen. 
Samalla funktiolla voidaan varata muistia ohjelman jokaiselle dynaamiselle rakenteelle.
*/
void *allocateMemory( int size );

/*
Ottaa k�ytt�j�lt� sy�tteen, hoitaa tyypillisen virheentarkistuksen ja lopuksi tarkistaa onko sy�te oikealta v�lilt�.
*/
int getInputInRange( int *target, const int rangeStart, const int rangeEnd );

/*
Tulostaa alkuvalikon.
*/
void printMenu();

/*
Palautusvalikko ja toiminto on omassa funktiossaan.
*/
void returnMenu( Bottle *root );

/*
Tulostaa kuitin n�yt�lle.
*/
void printRecipe( RetBottle *root, const int num, const float sum );

/*
Hakee aikaleiman char-merkkijonoon.
*/
void getTimeStamp( char *target);
